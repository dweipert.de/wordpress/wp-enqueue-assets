<?php

namespace Dweipert\WpEnqueueAssets;

trait ThemeUsesWordPressScripts
{
    use UsesWordPressScripts;

    /**
     * @param string $asset
     *
     * @return string
     */
    public function assetsUrl(string $asset): string
    {
        return get_stylesheet_directory_uri() . "/{$this->buildDir}/$asset";
    }

    /**
     * @param string $asset
     *
     * @return array
     */
    public function assetsMeta(string $asset): array
    {
        return include get_stylesheet_directory() . "/{$this->buildDir}/$asset.asset.php";
    }
}
