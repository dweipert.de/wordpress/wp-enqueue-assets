<?php

namespace Dweipert\WpEnqueueAssets;

trait PluginUsesWordPressScripts
{
    use UsesWordPressScripts;

    /**
     * Assuming the main plugin class file is in a sub-folder
     *
     * @var string
     */
    protected string $pluginDir = __DIR__;

    /**
     * @param string $asset
     *
     * @return string
     */
    public function assetsUrl(string $asset): string
    {
        return plugin_dir_url($this->pluginDir) . "{$this->buildDir}/$asset";
    }

    /**
     * @param string $asset
     *
     * @return array
     */
    public function assetsMeta(string $asset): array
    {
        return include plugin_dir_path($this->pluginDir) . "{$this->buildDir}/$asset.asset.php";
    }
}
