<?php

namespace Dweipert\WpEnqueueAssets;

trait UsesWordPressScripts
{
    /**
     * @var string
     */
    protected string $buildDir = 'build';

    /**
     * @param string $handle
     * @param string $asset
     * @param array $deps
     * @param bool $inFooter
     */
    public function enqueueScript(string $handle, string $asset, array $deps = [], bool $inFooter = false)
    {
        $assetBaseName = str_replace('.' . pathinfo($asset, PATHINFO_EXTENSION), '', $asset);
        $meta = $this->assetsMeta($assetBaseName);

        wp_enqueue_script(
            $handle,
            $this->assetsUrl($asset),
            array_replace($meta['dependencies'] ?? [], $deps),
            $meta['version'] ?? false,
            $inFooter
        );
    }

    /**
     * @param string $handle
     * @param string $asset
     * @param array $deps
     * @param string $media
     */
    public function enqueueStyle(string $handle, string $asset, array $deps = [], string $media = 'all')
    {
        $assetBaseName = str_replace('.' . pathinfo($asset, PATHINFO_EXTENSION), '', $asset);
        $meta = $this->assetsMeta($assetBaseName);

        wp_enqueue_style(
            $handle,
            $this->assetsUrl($asset),
            $deps,
            $meta['version'] ?? false,
            $media
        );
    }

    /**
     * @param string $asset
     *
     * @return string Url to asset in $buildDir
     */
    abstract public function assetsUrl(string $asset): string;

    /**
     * @param string $asset
     *
     * @return array Data from $asset.asset.php in $buildDir
     */
    abstract public function assetsMeta(string $asset): array;
}
